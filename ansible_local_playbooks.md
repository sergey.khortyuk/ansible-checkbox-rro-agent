
* using Ansible command line:

```bash
ansible-playbook --connection=local 127.0.0.1 playbook.yml
```

* using inventory:

```ini
127.0.0.1 ansible_connection=local
```

* using Ansible configuration file:

```ini
[defaults]
transport = local
```

* using playbook header:

```yaml
- hosts: 127.0.0.1
  connection: local
```
